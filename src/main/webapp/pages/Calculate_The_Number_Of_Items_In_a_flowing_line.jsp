<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Calculate_The_Number_Of_Items_In_a_flowing_line</title>
<script type="text/javascript" src="../js/jquery/jquery-1.8.3.min.js" /></script>
<style type="text/css">
#example {
  padding: 0 40px;
}

#example div {
  float: left;
  width: 150px;
  height: 150px;
  background: #eee;
  margin: 0 10px 10px 0;
}

.answer {
  font-weight: bold;
}
/* For aesthetics only */
body {
  margin: 0;
  font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}

.intro {
  padding: 40px;
}

.intro h1 {
  font: 200 1.7em Segoe UI, "Segoe UI Light", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
  font-weight: 200;
  color: #666;
}

.intro p {
  max-width: 600px;
}
</style>
<script type="text/javascript">
  $(function() {
    $("#answer").text(calculateItemsInALine($("#example").find("div")));
    $(window).on("resize", function() {
      $("#answer").text(calculateItemsInALine($("#example").find("div")));
    });
  });
  
  function calculateItemsInALine(items) {
    var itemOffset, numberOfItemsInLine;
    items.each(function(i) {
      var thisItem = $(this);
      if (i == 0) {
        itemOffset = thisItem.offset().top;
      }
      if (thisItem.offset().top == itemOffset) {
        numberOfItemsInLine = i + 1;
      }
    });
    return numberOfItemsInLine;
  }
</script>
</head>
<body>
  <div class="intro" style="background-color: #333333">
    <h1>Question: How many items are there in a line?</h1>
    <p class="answer">
      Answer: <span id="answer"></span>
    </p>
    <p>Shrink your browser window horizontally and watch it count the number of items in a line.</p>
  </div>
  <div id="example">

    <div>Item 1</div>
    <div>Item 2</div>
    <div>Item 3</div>
    <div>Item 4</div>
    <div>Item 5</div>
    <div>Item 6</div>
    <div>Item 7</div>
    <div>Item 8</div>
    <div>Item 9</div>
    <div>Item 10</div>
    <div>Item 11</div>
    <div>Item 12</div>
    <div>Item 13</div>
    <div>Item 14</div>
    <div>Item 15</div>
    <div>Item 16</div>
    <div>Item 17</div>
    <div>Item 18</div>
    <div>Item 19</div>
    <div>Item 20</div>
    <div>Item 21</div>
    <div>Item 22</div>
    <div>Item 23</div>
    <div>Item 24</div>
    <div>Item 25</div>
    <div>Item 26</div>
    <div>Item 27</div>
    <div>Item 28</div>
    <div>Item 29</div>
    <div>Item 30</div>
  </div>
</body>
</html>