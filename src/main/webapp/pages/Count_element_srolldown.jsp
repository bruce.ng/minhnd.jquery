<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Count Element Srolldown</title>

<!-- these things are here to make this page work -->

<!-- 
<script type="text/javascript" src="../js/jquery.dropdownReplacement-0.5/lib/jquery-1.4.2.min.js" /></script>
-->


<script type="text/javascript" src="../js/jquery/jquery-1.8.3.min.js" /></script>
<script type="text/javascript" src="../js/jquery/jquery-1.11.1.min.js" /></script>
<script>var $11 = jQuery.noConflict( true );</script>



<script type="text/javascript" src="../js/jquery.dropdownReplacement-0.5/lib/jquery-ui-1.8.2.custom.min.js" /></script>
<script type="text/javascript" src="../js/jquery.dropdownReplacement-0.5/lib/jquery.jgrowl_pack.js" /></script>
<script type="text/javascript" src="../js/jquery.dropdownReplacement-0.5/lib/malsup.js" /></script>

<link rel="stylesheet" href="../js/jquery.dropdownReplacement-0.5/lib/jquery-ui-1.8.2.custom.css" media="screen" />
<link rel="stylesheet" href="../js/jquery.dropdownReplacement-0.5/lib/subtabs.css" media="screen" />
<link rel="stylesheet" href="../js/jquery.dropdownReplacement-0.5/lib/jquery.jgrowl.css" media="screen" />


<link href='../js/jquery.dropdownReplacement-0.5/lib/shCore.css' rel='stylesheet' type='text/css' />
<link href='http://alexgorbatchev.com/pub/sh/current/styles/shThemeDefault.css' rel='stylesheet' type='text/css' />

<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shCore.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushCpp.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushCSharp.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushCss.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushJava.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushJScript.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushPhp.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushPython.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushRuby.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushSql.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushVb.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushXml.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushPerl.js' type='text/javascript'></script>
<script src='http://alexgorbatchev.com/pub/sh/current/scripts/shBrushGroovy.js' type='text/javascript'></script>

<!-- these are the only things you need for the dropdownReplacement plugin to run -->
<script type='text/javascript' src="../js/jquery.dropdownReplacement-0.5/jquery.scrollTo.js"></script>
<!-- required -->
<script type='text/javascript' src="../js/jquery.dropdownReplacement-0.5/jquery.bgiframe.min.js"></script>
<!-- optional: for better IE6 support -->
<script type='text/javascript' src="../js/jquery.dropdownReplacement-0.5/jquery.support.windowsTheme-0.3.js"></script>
<!-- optional: for better skinning support -->
<script type='text/javascript' src="../js/jquery.dropdownReplacement-0.5/jquery.dropdownReplacement-0.5-pack.js"></script>
<!-- required -->
<link rel="stylesheet" href="../js/jquery.dropdownReplacement-0.5/jquery.dropdownReplacement-0.5.css" media="screen" />
<!-- required -->
<!-- end things you need -->

<style>
#counter {
  position: relative;
  right: 1050px;
  top: 0px;;
  float: right;
  color: #1CD1D8;
  background-color: #444;
  border-radius: 20px;
  padding: 8px;
  min-width: 20px;
  text-align: center;
  display: none;
}

/** start ------------custom lai class cua the div*/
.divCustomSelect {
  display: block;
  width: 184px;
  height: 180px;
  top: 172px;
  left: 8px;
}

div.divCustomSelect {
  border: 1px solid #000;
  background-color: #FFF;
  padding: 0;
  display: none;
  position: absolute;
  overflow-y: auto;
  overflow-x: hidden;
  z-index: 50;
}

a.itemCustom {
  padding: 0 20px 0 2px;
  margin: 0;
  list-style: none;
  display: block;
  white-space: nowrap;
  text-decoration: none;
  color: #000;
  cursor: pointer;
  z-index: 100;
  text-align: left;
}

a.itemCustom:hover {
  background-color: #0A246A;
  color: #FFF;
}

/** end ------------custom lai class cua the div*/
</style>
</head>
<body>

<select id="customSelect" name="customSelect">
  <option value="1">Aachener Stra&#223;e</option><option value="2">Abbestra&#223;e</option><option value="3">Adalbertstra&#223;e</option><option value="4">Adam-von-Trott-Stra&#223;e</option><option value="5">Adenauerplatz</option><option value="6">Admiralstra&#223;e</option><option value="7">Agathe-Lasch-Platz</option><option value="8">Ahornallee</option><option value="9">Ahrensfelder Chaussee</option><option value="10">Ahrenshooper Stra&#223;e</option><option value="11">Ahrweilerstra&#223;e</option><option value="12">Akazienallee</option><option value="13">Albrecht-Achilles-Stra&#223;e</option><option value="14">Alemannenallee</option><option value="15">Alexandrinenstra&#223;e</option><option value="16">Alfred-D&#246;blin-Platz</option><option value="17">Allendorfer Weg</option><option value="18">Alte Allee</option><option value="19">Alte Brauerei</option><option value="20">Alte Jakobstra&#223;e</option><option value="21">Altenburger Allee</option><option value="22">Altenhofer Stra&#223;e</option><option value="23">Alt-Lietzow</option><option value="24">Alt-Stralau</option><option value="25">Am Bahnhof Grunewald</option><option value="26">Am Bahnhof Jungfernheide</option><option value="27">Am Bahnhof Westend</option><option value="28">Am Berl</option><option value="29">Am Breiten Luch</option><option value="30">Am Comeniusplatz</option><option value="31">Am Containerbahnhof</option><option value="32">Am D&#246;rferweg</option><option value="33">Am Dornbusch</option><option value="34">Am Faulen See</option><option value="35">Am Fliederbusch</option><option value="36">Am Glockenturm</option><option value="37">Am G&#252;terbahnhof Halensee</option><option value="38">Am Gutshof</option><option value="39">Am Hain</option><option value="40">Am Heidebusch</option><option value="41">Am Johannistisch</option><option value="42">Am Oberbaum</option><option value="43">Am Ostbahnhof</option><option value="44">Am Postbahnhof</option><option value="45">Am Postfenn</option><option value="46">Am Postfenn</option><option value="47">Am Rudolfplatz</option><option value="48">Am Rupenhorn</option><option value="49">Am Schillertheater</option><option value="50">Am Speicher</option><option value="51">Am Spreebord</option><option value="52">Am Tempelhofer Berg</option><option value="53">Am Vogelherd</option><option value="54">Am Volkspark</option><option value="55">Am Weinhang</option><option value="56">Am Westkreuz</option><option value="57">Am Wriezener Bahnhof</option><option value="58">Amselstra&#223;e</option><option value="59">Amtsgerichtsplatz</option><option value="60">An der Barthschen Promenade</option><option value="61">An der Brauerei</option><option value="62">An der Flie&#223;wiese</option><option value="63">An der Margaretenh&#246;he</option><option value="64">An der Michaelbr&#252;cke</option><option value="65">An der Ostbahn</option><option value="66">An der Schillingbr&#252;cke</option><option value="67">Andreasplatz</option><option value="68">Andreasstra&#223;e</option><option value="69">Angerburger Allee</option><option value="70">Anhalter Stra&#223;e</option><option value="71">Anna-Ebermann-Stra&#223;e</option><option value="72">Annemariestra&#223;e</option><option value="73">Arcostra&#223;e</option><option value="74">Arendsweg</option><option value="75">Arndtstra&#223;e</option><option value="76">Arnimstra&#223;e</option><option value="77">Arysallee</option><option value="78">Aschaffenburger Stra&#223;e</option><option value="79">Askanischer Platz</option><option value="80">A&#223;mannshauser Stra&#223;e</option><option value="81">Astridstra&#223;e</option><option value="82">Auerbacher Stra&#223;e</option><option value="83">Auerstra&#223;e</option><option value="84">Augsburger Stra&#223;e</option><option value="85">Augsburger Stra&#223;e</option><option value="86">Augustastra&#223;e</option><option value="87">Auguste-Viktoria-Stra&#223;e</option><option value="88">Avus</option><option value="89">Avus</option><option value="90">Axel-Springer-Stra&#223;e</option><option value="91">Babelsberger Stra&#223;e</option><option value="92">Badenallee</option><option value="93">Badensche Stra&#223;e</option><option value="94">Baerwaldstra&#223;e</option><option value="95">Bahnhofstra&#223;e</option><option value="96">Bahrfeldtstra&#223;e</option><option value="97">Ballenstedter Stra&#223;e</option><option value="98">Bamberger Stra&#223;e</option><option value="99">B&#228;nschstra&#223;e</option><option value="100">Barbarossastra&#223;e</option>
</select>
<div id="counter"></div>

<select id="defaultSelect" name="defaultSelect">
  <option value="1">Aachener Stra&#223;e</option><option value="2">Abbestra&#223;e</option><option value="3">Adalbertstra&#223;e</option><option value="4">Adam-von-Trott-Stra&#223;e</option><option value="5">Adenauerplatz</option><option value="6">Admiralstra&#223;e</option><option value="7">Agathe-Lasch-Platz</option><option value="8">Ahornallee</option><option value="9">Ahrensfelder Chaussee</option><option value="10">Ahrenshooper Stra&#223;e</option><option value="11">Ahrweilerstra&#223;e</option><option value="12">Akazienallee</option><option value="13">Albrecht-Achilles-Stra&#223;e</option><option value="14">Alemannenallee</option><option value="15">Alexandrinenstra&#223;e</option><option value="16">Alfred-D&#246;blin-Platz</option><option value="17">Allendorfer Weg</option><option value="18">Alte Allee</option><option value="19">Alte Brauerei</option><option value="20">Alte Jakobstra&#223;e</option><option value="21">Altenburger Allee</option><option value="22">Altenhofer Stra&#223;e</option><option value="23">Alt-Lietzow</option><option value="24">Alt-Stralau</option><option value="25">Am Bahnhof Grunewald</option><option value="26">Am Bahnhof Jungfernheide</option><option value="27">Am Bahnhof Westend</option><option value="28">Am Berl</option><option value="29">Am Breiten Luch</option><option value="30">Am Comeniusplatz</option><option value="31">Am Containerbahnhof</option><option value="32">Am D&#246;rferweg</option><option value="33">Am Dornbusch</option><option value="34">Am Faulen See</option><option value="35">Am Fliederbusch</option><option value="36">Am Glockenturm</option><option value="37">Am G&#252;terbahnhof Halensee</option><option value="38">Am Gutshof</option><option value="39">Am Hain</option><option value="40">Am Heidebusch</option><option value="41">Am Johannistisch</option><option value="42">Am Oberbaum</option><option value="43">Am Ostbahnhof</option><option value="44">Am Postbahnhof</option><option value="45">Am Postfenn</option><option value="46">Am Postfenn</option><option value="47">Am Rudolfplatz</option><option value="48">Am Rupenhorn</option><option value="49">Am Schillertheater</option><option value="50">Am Speicher</option><option value="51">Am Spreebord</option><option value="52">Am Tempelhofer Berg</option><option value="53">Am Vogelherd</option><option value="54">Am Volkspark</option><option value="55">Am Weinhang</option><option value="56">Am Westkreuz</option><option value="57">Am Wriezener Bahnhof</option><option value="58">Amselstra&#223;e</option><option value="59">Amtsgerichtsplatz</option><option value="60">An der Barthschen Promenade</option><option value="61">An der Brauerei</option><option value="62">An der Flie&#223;wiese</option><option value="63">An der Margaretenh&#246;he</option><option value="64">An der Michaelbr&#252;cke</option><option value="65">An der Ostbahn</option><option value="66">An der Schillingbr&#252;cke</option><option value="67">Andreasplatz</option><option value="68">Andreasstra&#223;e</option><option value="69">Angerburger Allee</option><option value="70">Anhalter Stra&#223;e</option><option value="71">Anna-Ebermann-Stra&#223;e</option><option value="72">Annemariestra&#223;e</option><option value="73">Arcostra&#223;e</option><option value="74">Arendsweg</option><option value="75">Arndtstra&#223;e</option><option value="76">Arnimstra&#223;e</option><option value="77">Arysallee</option><option value="78">Aschaffenburger Stra&#223;e</option><option value="79">Askanischer Platz</option><option value="80">A&#223;mannshauser Stra&#223;e</option><option value="81">Astridstra&#223;e</option><option value="82">Auerbacher Stra&#223;e</option><option value="83">Auerstra&#223;e</option><option value="84">Augsburger Stra&#223;e</option><option value="85">Augsburger Stra&#223;e</option><option value="86">Augustastra&#223;e</option><option value="87">Auguste-Viktoria-Stra&#223;e</option><option value="88">Avus</option><option value="89">Avus</option><option value="90">Axel-Springer-Stra&#223;e</option><option value="91">Babelsberger Stra&#223;e</option><option value="92">Badenallee</option><option value="93">Badensche Stra&#223;e</option><option value="94">Baerwaldstra&#223;e</option><option value="95">Bahnhofstra&#223;e</option><option value="96">Bahrfeldtstra&#223;e</option><option value="97">Ballenstedter Stra&#223;e</option><option value="98">Bamberger Stra&#223;e</option><option value="99">B&#228;nschstra&#223;e</option><option value="100">Barbarossastra&#223;e</option>
</select>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<input class="demo5" type="text" value="Altenhofer Stra&#223;e" />
<script type="text/javascript">
  
  var displaySize = 10;
  var $select = $('#customSelect');
  var $counter = $('#counter');
  
  console.log('customSelect', $select);
  console.log('counter', $counter);
  
  $counter.text(displaySize);
  $counter.show();
  
  
  $("#customSelect").dropdownReplacement({
    selectClass : "inputCustomSelect",
    optionsClass : "divCustomSelect",
    optionsDisplayNum : displaySize,
    optionClass : "itemCustom"
  });
  
  
  
  
  
  var topItemFirst;
  $('.inputCustomSelect').click(function(e){
    topItemFirst = $('#itemCustom1').offset().top;
    console.log('topItemFirst', topItemFirst)
  })
  
  
  var lastScrollTop = 0;
  $(".divCustomSelect").scroll(function(e) {
    var st = $(this).scrollTop();
    
    if (st > lastScrollTop) {
       console.log('down--Scroll', st, 'lastScrollTop', lastScrollTop);
       var numberOfItemsInLine = calculateItemsInALine($(".divCustomSelect").find("a"), topItemFirst);
       $('#counter').text(numberOfItemsInLine);
       console.log('numberOfItemsInLine=' + numberOfItemsInLine);
    } else {
      console.log('up-----Scroll', st);
    }
    
    lastScrollTop = st;
    
  }).on('change blur', function() {
    console.log('change blur');
    $counter.hide();
    
  }).on('mousedown', function() {
    console.log('mousedown');
  });
  
  var topInputCustomSelect = $('.inputCustomSelect').offset().top + 1;
  console.log('topInputCustomSelect', topInputCustomSelect);
  
  function calculateItemsInALine(items, topItemFirst) {
    console.log('---calculateItemsInALine--');
    
    var divSelectHeight = $('.divCustomSelect').height();
    var divSelectTop = $('.divCustomSelect').offset().top;
    console.log('divCustomSelect 1.height', divSelectHeight, '2.top', divSelectTop);
    
    //item height = 18, item1 offset top = 27
    var itemHeight = $('#itemCustom1').height();
    var topCustomSelect = $('#customSelect').offset().top;
    var topItemOne = $('#itemCustom1').offset().top;
    var topItemOneAbs = Math.abs(topItemOne);
    
    
    var itemOffset, numberOfItemsInLine;
    
    //itemCustom10 189
    
    //for toan bo items
    items.each(function(i) {
      
      var thisItem = $(this);
      var itemTop = thisItem.offset().top;
      //1 la sai số
      if (itemTop < (divSelectHeight + divSelectTop)) {
        //console.log('1', itemTop, '2', divSelectHeight + divSelectTop);
        //bat dau tinh toan bao nhieu phan tu xuat hien
        //27 - 9
        numberOfItemsInLine = (itemTop + topItemOneAbs - topItemFirst - topInputCustomSelect) / itemHeight;
        if (topItemOne < 0) {
          numberOfItemsInLine = numberOfItemsInLine + 3;
        }
      } else {
        //phần tử chưa xuất hiện ko tính toán 
        return;
      }
    });
    return numberOfItemsInLine;
  };
  
  
$(".demo5").dropdownReplacement({
  selectClass : "divDemo5",
  optionsDisplayNum : 10,
  options : [{"t":"Aachener Straße","v":"1"},{"t":"Abbestraße","v":"2"},{"t":"Adalbertstraße","v":"3"},{"t":"Adam-von-Trott-Straße","v":"4"},{"t":"Adenauerplatz","v":"5"},{"t":"Admiralstraße","v":"6"},{"t":"Agathe-Lasch-Platz","v":"7"},{"t":"Ahornallee","v":"8"},{"t":"Ahrensfelder Chaussee","v":"9"},{"t":"Ahrenshooper Straße","v":"10"},{"t":"Ahrweilerstraße","v":"11"},{"t":"Akazienallee","v":"12"},{"t":"Albrecht-Achilles-Straße","v":"13"},{"t":"Alemannenallee","v":"14"},{"t":"Alexandrinenstraße","v":"15"},{"t":"Alfred-Döblin-Platz","v":"16"},{"t":"Allendorfer Weg","v":"17"},{"t":"Alte Allee","v":"18"},{"t":"Alte Brauerei","v":"19"},{"t":"Alte Jakobstraße","v":"20"},{"t":"Altenburger Allee","v":"21"},{"t":"Altenhofer Straße","v":"22"},{"t":"Alt-Lietzow","v":"23"},{"t":"Alt-Stralau","v":"24"},{"t":"Am Bahnhof Grunewald","v":"25"},{"t":"Am Bahnhof Jungfernheide","v":"26"},{"t":"Am Bahnhof Westend","v":"27"},{"t":"Am Berl","v":"28"},{"t":"Am Breiten Luch","v":"29"},{"t":"Am Comeniusplatz","v":"30"},{"t":"Am Containerbahnhof","v":"31"},{"t":"Am Dörferweg","v":"32"},{"t":"Am Dornbusch","v":"33"},{"t":"Am Faulen See","v":"34"},{"t":"Am Fliederbusch","v":"35"},{"t":"Am Glockenturm","v":"36"},{"t":"Am Güterbahnhof Halensee","v":"37"},{"t":"Am Gutshof","v":"38"},{"t":"Am Hain","v":"39"},{"t":"Am Heidebusch","v":"40"},{"t":"Am Johannistisch","v":"41"},{"t":"Am Oberbaum","v":"42"},{"t":"Am Ostbahnhof","v":"43"},{"t":"Am Postbahnhof","v":"44"},{"t":"Am Postfenn","v":"45"},{"t":"Am Postfenn","v":"46"},{"t":"Am Rudolfplatz","v":"47"},{"t":"Am Rupenhorn","v":"48"},{"t":"Am Schillertheater","v":"49"},{"t":"Am Speicher","v":"50"},{"t":"Am Spreebord","v":"51"},{"t":"Am Tempelhofer Berg","v":"52"},{"t":"Am Vogelherd","v":"53"},{"t":"Am Volkspark","v":"54"},{"t":"Am Weinhang","v":"55"},{"t":"Am Westkreuz","v":"56"},{"t":"Am Wriezener Bahnhof","v":"57"},{"t":"Amselstraße","v":"58"},{"t":"Amtsgerichtsplatz","v":"59"},{"t":"An der Barthschen Promenade","v":"60"},{"t":"An der Brauerei","v":"61"},{"t":"An der Fließwiese","v":"62"},{"t":"An der Margaretenhöhe","v":"63"},{"t":"An der Michaelbrücke","v":"64"},{"t":"An der Ostbahn","v":"65"},{"t":"An der Schillingbrücke","v":"66"},{"t":"Andreasplatz","v":"67"},{"t":"Andreasstraße","v":"68"},{"t":"Angerburger Allee","v":"69"},{"t":"Anhalter Straße","v":"70"},{"t":"Anna-Ebermann-Straße","v":"71"},{"t":"Annemariestraße","v":"72"},{"t":"Arcostraße","v":"73"},{"t":"Arendsweg","v":"74"},{"t":"Arndtstraße","v":"75"},{"t":"Arnimstraße","v":"76"},{"t":"Arysallee","v":"77"},{"t":"Aschaffenburger Straße","v":"78"},{"t":"Askanischer Platz","v":"79"},{"t":"Aßmannshauser Straße","v":"80"},{"t":"Astridstraße","v":"81"},{"t":"Auerbacher Straße","v":"82"},{"t":"Auerstraße","v":"83"},{"t":"Augsburger Straße","v":"84"},{"t":"Augsburger Straße","v":"85"},{"t":"Augustastraße","v":"86"},{"t":"Auguste-Viktoria-Straße","v":"87"},{"t":"Avus","v":"88"},{"t":"Avus","v":"89"},{"t":"Axel-Springer-Straße","v":"90"},{"t":"Babelsberger Straße","v":"91"},{"t":"Badenallee","v":"92"},{"t":"Badensche Straße","v":"93"},{"t":"Baerwaldstraße","v":"94"},{"t":"Bahnhofstraße","v":"95"},{"t":"Bahrfeldtstraße","v":"96"},{"t":"Ballenstedter Straße","v":"97"},{"t":"Bamberger Straße","v":"98"},{"t":"Bänschstraße","v":"99"},{"t":"Barbarossastraße","v":"100"}, ],
  onSelect : function(value, text, selectIndex) {
    console.log("value:" + value + " text:" + text + " index:" + selectIndex);
  }
});

$(".divDemo5").scroll(function(e) {
  console.log('scroll demo5');
}).on('change blur', function() {
  console.log('change blur');
}).on('mousedown', function() {
  console.log('mousedown');
});
    </script>
</body>
</html>